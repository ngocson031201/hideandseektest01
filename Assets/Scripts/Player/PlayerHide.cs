﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerHide : MonoBehaviour
{
    Rigidbody rg;
    [SerializeField] ParticleSystem EffectWalkThroughWall;
    public NavMeshAgent agent;
    PlayerController PlayerController;
    private void Start()
    {
        rg = GetComponent<Rigidbody>();
        PlayerController = GetComponent<PlayerController>();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "sensor" && PlayerController.checkBulletInvisibility==false)
        {
            PlayerController.instance.DisableJoystick();
            PlayerController.instance.loseAnim();
            UIController.instance.EndTimeLose();
        }

        //if (other.gameObject.tag == "CheckPlayerHide")
        //{
        //    PlayerController.instance.DisableJoystick();
        //    PlayerController.instance.loseAnim();
        //    UIController.instance.EndTimeLose();
        //}

    }

}
