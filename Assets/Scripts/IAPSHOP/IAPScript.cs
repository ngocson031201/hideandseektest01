using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IAPScript : MonoBehaviour
{
    
    public void BagOfCoin()
    {
        ScoreManager.instance.AddCoinDaily(10000);
    }
    
    public void JackPot()
    {
        ScoreManager.instance.AddCoinDaily(20000);
    }

    public void WatchVideoCoinReward()
    {
        ManagerAds.ins.ShowRewarded((x) =>
        {
            if (x)
            {
                ScoreManager.instance.AddCoinDaily(100);
            }
        });
    }



    public void AddAallSkin()
    {
        Prefs.CHECKUNLOCKALLSKIN++;
        Debug.Log("CHECKUNLOCKALLSKIN" + Prefs.CHECKUNLOCKALLSKIN);
        ProfileShopUI.instance.UnLockAllSkin();

    }

    public void StarterPack()
    {
        Prefs.CHECKSTARTERPACK++;
        Debug.Log("CHECKSTARTERPACK" + Prefs.CHECKSTARTERPACK);
        ProfileShopUI.instance.StarterPack();

    }

    public void PrivicyPolicy()
    {
        ManagerAds.ins.ShowPrivacyPolicy();
    }
}
